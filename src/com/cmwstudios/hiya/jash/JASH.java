package com.cmwstudios.hiya.jash;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class JASH implements JASHSystem {
    public String dir;

    public static void main(String[] args) throws IOException {
        print(releaseName);
        print(releaseCode);
        prompt("/home/aidan", PrivilageLevel.USER);
    }

    public static void sys(String s) {
        String j;
        Process p;
        try {
            p = Runtime.getRuntime().exec(s);
            BufferedReader br = new BufferedReader(
                    new InputStreamReader(p.getInputStream()));
            while ((j = br.readLine()) != null)
                System.out.println(j);
            p.waitFor();
        } catch (Exception e) {
        }
    }

    public static void print(String s) {
        System.out.println(s);
    }

    public static void prompt(String dir, PrivilageLevel priv) throws IOException {
        System.out.append(dir + " $ ");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String s = br.readLine();
        String j = s.replace(dir + " $", "");
        sys(j);
        sys("java -jar " + j);
        prompt(dir, priv);
            }

        }