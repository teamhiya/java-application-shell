package com.cmwstudios.hiya.jash;

public enum PrivilageLevel {
    ROOT, USER
}
