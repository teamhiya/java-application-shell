package com.cmwstudios.hiya.jash;

public interface JASHSystem {
    String releaseCode = "AS-1.1";
    String releaseName = "Java Application Shell Alpha Series Version 1.1";
}